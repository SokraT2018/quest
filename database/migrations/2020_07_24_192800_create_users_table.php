<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $customerRoleId = 1;

        Schema::create('users', function (Blueprint $table) use ($customerRoleId){
            $table->id();
            $table->string('name')->unique();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->rememberToken();
            $table->string('password');
            $table->unsignedInteger('role_id')->default($customerRoleId);
            $table->unsignedInteger('team_id')->nullable();
            $table->timestamps();
            //$table->foreign('role_id')->references('id')->on('user_roles')->onDelete('set null')->onUpdate('cascade');
            //$table->foreign('team_id')->references('id')->on('teams')->onDelete('set null')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
